from django.urls import path

from corporate_pages import views

urlpatterns = [
	path('page/<int:pk>', views.corporate_page, name = 'corporate_page'),
	path('contact/', views.contact_page, name = 'contact_page'),
	path('register/', views.register_page, name = 'register_page'),
	]
