from django.db import models


# Create your models here.

class CorporatePage(models.Model):
	title = models.CharField(max_length = 255)
	content = models.TextField(null = True, blank = True)
	attachment = models.FileField(null = True, blank = True, upload_to = 'uploads/corporate/')
	date_added = models.DateTimeField(auto_now_add = True)
	date_modified = models.DateTimeField(auto_now = True)

	def __str__(self):
		return self.title


class ContactPage(models.Model):
	name = models.CharField(max_length = 255)
	message = models.TextField()
	email = models.EmailField()

	def __str__(self):
		return self.name
