from django.shortcuts import render, get_object_or_404

from corporate_pages.forms import ContactForm
from corporate_pages.models import CorporatePage


def corporate_page(request, pk):
	corporate = get_object_or_404(CorporatePage, pk = pk)
	return render(request, 'corporate_pages/corporate_page.html', {'corporate': corporate})


def contact_page(request):
	if request.method == 'POST':
		form = ContactForm(request.POST)
		if form.is_valid():
			success = 'Your message has been sent. Thank you'
			form.cleaned_data['name'],
			form.cleaned_data['email'],
			form.cleaned_data['message'],
			form.save()
		# messages.success(request, f'Your message has been sent. Thank you')
			return render(request, 'corporate_pages/contact.html', {'success': success})
	else:
		form = ContactForm()
	return render(request, 'corporate_pages/contact.html', {'contact': form})


def register_page(request):
	return None