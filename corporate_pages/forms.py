from django import forms
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

from corporate_pages.models import ContactPage


class ContactForm(ModelForm):
	class Meta:
		model = ContactPage
		fields = '__all__'  # get all the fields in the form
		localized_fields = '__all__'
		widgets = {
			'name': forms.TextInput(attrs = {'class': 'input is-medium', }),
			'email': forms.TextInput(attrs = {'class': 'input is-medium', }),
			'message': forms.Textarea(attrs = {'class': 'textarea is-medium', 'cols': '', 'rows': ''}),
			}

	help_texts = {
		'name': _('Your name'),
		'email': _('Email address where we can write back to you.'),
		'message': _('Your Message'),
		}
	error_messages = {
		'name': {
			'max_length': _("Your name is too long"),
			},
		}
