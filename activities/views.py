# Create your views here.
from django.shortcuts import render, get_object_or_404

from activities.forms import ActivityForm
from activities.models import Activity


def activity_list(request):
	activities = Activity.objects.all()
	return render(request, 'activities/activity_list.html', {'activities': activities})


def activity_detail(request, pk):
	activity = get_object_or_404(Activity, pk = pk)
	return render(request, 'activities/activity_detail.html', {'activity': activity})


def crate_activity(request):
	if request.method == 'POST':
		form = ActivityForm(request.POST, request.FILES)
		if form.is_valid():
			saved_details = form.save(commit = False)
			saved_details.name = request.user
			form.cleaned_data['title']
			form.cleaned_data['activity_description']
			success = 'Good Job'
			saved_details.save()
			return render(request, 'corporate_pages/contact.html', {'success': success})
	else:
		form = ActivityForm()
	return render(request, 'activities/create_activity.html', {'activity_form': form})
