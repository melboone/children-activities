from django.urls import path

from . import views

urlpatterns = [
	path('', views.activity_list, name = 'activity_list'),
	path('activity/<int:pk>', views.activity_detail, name = 'activity_detail'),
	path('activity/create/', views.crate_activity, name = 'create_activity'),
	]
