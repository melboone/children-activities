from django.core.exceptions import ValidationError
from django.db import models

# Create your models here.

activity_age = (
	('1', 'under 5'),
	('2', 'Between 5 and 10'),
	('3', '10 - 18'),
	('4', '18+'),
	)


def validate_file_extension(value):
	import os
	extension = os.path.splitext(value.name)[1]
	not_accepted_extensions = ['.scf', '.link', '.inf', '.reg', '.ws', '.wsf', '.wsc', '.wsh', 'ps1', '.ps1', '.xml', '.ps2', '.ps2xml', '.psc1', '.psc2', '.msh', '.msh1', '.msh2',
	                           '.mshxml', '.msh1xml', '.msh2xml', '.js', '.jse', '.vb', '.vbs', '.vbe', '.cmd', '.bat', '.jar', '.exe', '.pif', '.application', '.gadget', '.msi',
	                           '.msp', '.com', '.scr', '.hta', '.cpl', '.msc', '.m4a','zip','rar',]
	if extension in not_accepted_extensions:
		raise ValidationError('File not supported!')


class Activity(models.Model):
	title = models.CharField(max_length = 255, null = False, blank = False)
	created = models.DateTimeField(auto_now_add = True)
	last_update = models.DateTimeField(auto_now = True)
	activity_description = models.TextField()
	email = models.EmailField(null = True, blank = True)
	name = models.CharField(max_length = 255, null = True, blank = True)
	activity_image = models.ImageField(null = True, blank = True, upload_to = 'activities/images/')
	activity_attachment = models.FileField(null = True, blank = True, upload_to = 'activities/attachment', validators = [validate_file_extension])

	# activity_age = models.CharField(null = False, choices = activity_age, blank = False)
	# activity_type = models.CharField(null = False, blank = False)

	def __str__(self):
		return self.title

# todo add views per post
# todo add admin page for users
# todo add forum
