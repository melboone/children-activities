activities\_project package
===========================

Submodules
----------

activities\_project.asgi module
-------------------------------

.. automodule:: activities_project.asgi
   :members:
   :undoc-members:
   :show-inheritance:

activities\_project.settings module
-----------------------------------

.. automodule:: activities_project.settings
   :members:
   :undoc-members:
   :show-inheritance:

activities\_project.urls module
-------------------------------

.. automodule:: activities_project.urls
   :members:
   :undoc-members:
   :show-inheritance:

activities\_project.wsgi module
-------------------------------

.. automodule:: activities_project.wsgi
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: activities_project
   :members:
   :undoc-members:
   :show-inheritance:
