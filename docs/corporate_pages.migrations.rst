corporate\_pages.migrations package
===================================

Submodules
----------

corporate\_pages.migrations.0001\_initial module
------------------------------------------------

.. automodule:: corporate_pages.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

corporate\_pages.migrations.0002\_auto\_20200530\_1949 module
-------------------------------------------------------------

.. automodule:: corporate_pages.migrations.0002_auto_20200530_1949
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: corporate_pages.migrations
   :members:
   :undoc-members:
   :show-inheritance:
