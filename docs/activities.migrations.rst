activities.migrations package
=============================

Submodules
----------

activities.migrations.0001\_initial module
------------------------------------------

.. automodule:: activities.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

activities.migrations.0002\_auto\_20200530\_1949 module
-------------------------------------------------------

.. automodule:: activities.migrations.0002_auto_20200530_1949
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: activities.migrations
   :members:
   :undoc-members:
   :show-inheritance:
