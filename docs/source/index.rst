.. Activities documentation master file, created by
   sphinx-quickstart on Sat May 30 21:19:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Activities's documentation!
======================================

Documentation for the Code
**************************
.. toctree::
   :maxdepth: 2

   modules


==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

`reStructuredText syntax <https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>`_.